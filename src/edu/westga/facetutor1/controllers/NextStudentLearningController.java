/**
 * 
 */
package edu.westga.facetutor1.controllers;


import edu.westga.facetutor1.model.DataMap;
import edu.westga.facetutor1.model.FaceTutorData;

/**
 * Progresses to the next student image while
 * in learning mode
 * 
 * @author Thomas Hightower
 * @version 1.0
 */
public class NextStudentLearningController {
	private int current = 0;
	
	/**
	 * Creates a new NextStudentLearningController to
	 * progress to the next student image in the map
	 * 
	 */
	public NextStudentLearningController() {
	}

	/**
	 * Finds the next face to appear on the screen
	 * in learning mode
	 * 
	 * @param theMap
	 * @return	nextFace	the next face to appear
	 */
	public String goNext(DataMap<String, FaceTutorData> theMap) {
		String nextFace = new String();
		if (this.current < theMap.size() && this.current >= 0) {			
			nextFace = theMap.get(theMap.keys().get(this.current)).getName();
		} else if (this.current >= theMap.size() && this.current >= 0) {
			this.current = 0;
			nextFace = theMap.get(theMap.keys().get(this.current)).getName();
		}
		current++;
		System.out.println(nextFace);
		return nextFace;	
	}
}
