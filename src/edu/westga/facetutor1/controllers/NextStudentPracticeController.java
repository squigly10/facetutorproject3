package edu.westga.facetutor1.controllers;

import java.util.Random;

import edu.westga.facetutor1.model.DataMap;
import edu.westga.facetutor1.model.FaceTutorData;

/**
 * Progresses to the next student image
 * while in practice mode (random)
 * 
 * @author Thomas Hightower
 *
 */
public class NextStudentPracticeController {

	private Random randomizer;
	
	/**
	 * Creates a new NextStudentPracticeController to
	 * progress to a random student image in the map
	 * 
	 */
	public NextStudentPracticeController() {
		this.randomizer = new Random();
	}
	
	/**
	 * Finds the next face to appear on the screen
	 * in practice mode
	 * 
	 * @param theMap
	 * @return	nextFace	the next face to appear
	 */
	public String goNext(DataMap<String, FaceTutorData> theMap) {
		int randomLocation = this.randomizer.nextInt(theMap.size() - 1) + 1;
		String nextFace = theMap.keys().get(randomLocation);
		return nextFace;
	}
}
