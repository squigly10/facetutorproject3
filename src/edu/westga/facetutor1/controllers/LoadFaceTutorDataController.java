package edu.westga.facetutor1.controllers;

import java.io.File;
import java.io.IOException;
import java.util.List;

import edu.westga.facetutor1.datatier.TextFileLoader;
import edu.westga.facetutor1.datatier.TextLoader;
import edu.westga.facetutor1.model.DataMap;
import edu.westga.facetutor1.model.FaceTutorData;

/**
 * LoadFaceTutorDataController manages the "load student data from file" use case:
 * reading student data from a csv file and storing it in the data map.
 * 
 * @author Thomas Hightower
 * @version 1.0
 */
public class LoadFaceTutorDataController {

	private DataMap<String, FaceTutorData> theMap;
	private File inputFile;

	/**
	 * Creates a new LoadFaceTutorController to manage adding students to the specified
	 * map.
	 * 
	 * @precondition inputFile != null && aMap != null
	 * @postcondition the object is ready to get the data from the file
	 * 
	 * @param inputFile
	 *            the csv file to read
	 * @param aMap
	 * 			  the data map to which students will be added
	 */
	public LoadFaceTutorDataController(File inputFile,
									DataMap<String, FaceTutorData> aMap) {
		if (inputFile == null) {
			throw new IllegalArgumentException("File to load is null");
		}
		if (aMap == null) {
			throw new IllegalArgumentException("The map was null");
		}

		this.inputFile = inputFile;
		this.theMap = aMap;
	}

	/**
	 * Loads the contents of the csv file to the map.
	 * 
	 * @precondition none
	 * @postcondition the map contains student data
	 * 
	 * @throws IOException
	 *             if the file cannot be read
	 */
	public void loadFaceTutorData() throws IOException {
		TextLoader loader = new TextFileLoader(this.inputFile);
		List<String> linesFromFile = loader.loadText();

		for (String oneLineFromFile : linesFromFile) {
			this.addStudent(oneLineFromFile);
		}
	}

	private void addStudent(String oneLineFromFile) {
		int location = oneLineFromFile.indexOf('|');
		String[] faceTutorDataArray = { oneLineFromFile.substring(0, location), 
										oneLineFromFile.substring(location + 1) };
		FaceTutorData data = new FaceTutorData(faceTutorDataArray[0],
									   faceTutorDataArray[1]);
		this.theMap.add(data.getName(), data);
	}
	
	public DataMap<String, FaceTutorData> getTheMap() {
		return this.theMap;
		
	}
}