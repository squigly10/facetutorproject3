package edu.westga.facetutor1.controllers;

import edu.westga.facetutor1.model.DataMap;
import edu.westga.facetutor1.model.FaceTutorData;


/**
 * Checks if the selected name matches
 * the current image
 * 
 * @author Thomas Hightower
 * @version 1.0
 */
public class MatchPictureAndNameController {

	public MatchPictureAndNameController() {
		
	}

	public boolean checkIfMatch(String currentUrl, String selectedName, DataMap<String, FaceTutorData> theMap) {
		String urlOfCurrentName = theMap.get(selectedName).getFaceUrl();
		if(currentUrl == urlOfCurrentName) {
			return true;
		} else {
			return false;
		}
	}
}
