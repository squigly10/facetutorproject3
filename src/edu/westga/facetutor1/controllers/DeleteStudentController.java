/**
 * 
 */
package edu.westga.facetutor1.controllers;

import edu.westga.facetutor1.model.DataMap;
import edu.westga.facetutor1.model.FaceTutorData;

/**
 * Deletes the current student from
 * the DataMap
 * 
 * @author Thomas Hightower
 * @version 1.0
 */
public class DeleteStudentController {

	public DeleteStudentController() {
		
	}
	
	public DataMap<String, FaceTutorData> DeleteStudent(String selectedName, DataMap<String, FaceTutorData> theMap) {
		DataMap<String, FaceTutorData> newMap = theMap;
		newMap.remove(selectedName, newMap.get(selectedName));
		return newMap;
		
	}
}
