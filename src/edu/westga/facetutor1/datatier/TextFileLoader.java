package edu.westga.facetutor1.datatier;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;

/**
 * Loads data from a filesystem into memory.
 * 
 * @author Thomas Hightower
 * @version 1.0
 */
public class TextFileLoader implements TextLoader{

	private final File aTextFile;

	/**
	 * Creates new TextDataLoader for inputing the specified text file.
	 * 
	 * @precondition aTextFile != null
	 * @postcondition the object is ready to read or save to the file
	 * 
	 * @param aTextFile
	 * 		  the I/O file
	 */
	public TextFileLoader(File aTextFile) {
		if (aTextFile == null) {
			throw new IllegalArgumentException("File to load is null");
		}
		
		this.aTextFile = aTextFile;
	}

	/* (non-Javadoc)
	 * @see edu.westga.groups.datatier.TextLoader#loadText()
	 */
	@Override
	public List<String> loadText() throws IOException {
		return Files.readAllLines(this.aTextFile.toPath());
	}
}

