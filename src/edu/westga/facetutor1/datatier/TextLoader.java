package edu.westga.facetutor1.datatier;

import java.io.IOException;
import java.util.List;

/**
 * TextLoader defines the interface for classes that load
 * the contents of a specified data resource into memory.
 * 
 * @author Thomas Hightower
 * @version 1.0
 */
public interface TextLoader {

	/**
	 * Reads all lines from the data source.
	 * 
	 * @precondition none
	 * @return a list of Strings containing the contents of the data source
	 * 
	 * @throws IOException
	 *             if the data source can't be read
	 */
	List<String> loadText() throws IOException;

}