package edu.westga.facetutor1.view;

import java.io.File;
import java.io.IOException;

import javax.swing.JOptionPane;

import edu.westga.facetutor1.view.FaceTutorViewModel;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ListView;
import javafx.scene.control.MenuItem;
import javafx.scene.image.ImageView;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;

/**
 * FaceTutorGuiCodeBehind defines the "controller" for FaceTutorGui.fxml
 * 
 * @author Thomas Hightower
 * @version 1.0
 */
public class FaceTutorGuiCodeBehind {
	
	private static final String LOAD_FACETUTOR_DATA_TITLE = 
					"Load facetutor data";
	private static final String SUCCESSFUL_LOAD_MESSAGE = 
					"Successfully loaded student data";
	private static final String READ_DATA_ERROR_MESSAGE = 
					"ERROR: couldn't load the student data";
	private static final String ERROR_DIALOG_TITLE = 
					"Data import error";
	private static final String ABOUT_PROGRAM_DIALOGUE =
					"FaceTutor v1.0, �Thomas Hightower 2015";
	private static final String CORRECT_NAME =
			", That name is correct!";
	private static final String INCORRECT_NAME =
			", That name is incorrect.";
	private static final String ABOUT_FACETUTOR_TITLE = 
			"About Facetutor";
	private static final String NAME_PICTURE_MATCH_TITLE= 
			"Student Matching";
	
	
	private FaceTutorViewModel theViewModel;

    @FXML
    private Button deleteCurrentButton;

    @FXML
    private MenuItem aboutMenuItem;

    @FXML
    private MenuItem closeMenuItem;

    @FXML
    private ImageView studentImageView;

    @FXML
    private Button nextFaceButton;

    @FXML
    private MenuItem openClassMenuItem;

    @FXML
    private ListView<String> namesListView;
    
    @FXML
    private CheckBox learningCheckBox;
    
    /**
	 * Creates a new FaceTutorGuiCodeBehind object with its view model.
	 * 
	 * @precondition none
	 * @postcondition the object and its view model are ready to be initialized
	 */
    public FaceTutorGuiCodeBehind() {
    	this.theViewModel = new FaceTutorViewModel();
    }
    
    /**
	 * Initializes the GUI components, binding them to the view model properties
	 * and setting their event handlers.
	 */
	@FXML
	public void initialize() {
		this.bindGuiComponentsToViewModel();
		this.setEventActions();
	}

	private void setEventActions() {
		this.nextFaceButton.setOnAction(event -> this.handleNextFaceAction());
		this.openClassMenuItem.setOnAction(event -> this.handleImportClassRollAction());
		this.closeMenuItem.setOnAction(event -> Platform.exit());
		this.namesListView.setOnMouseClicked(event -> this.handleNameSelected());
		this.aboutMenuItem.setOnAction(event -> this.handleAboutProgram());
		this.deleteCurrentButton.setOnAction(event -> this.handleDelete());
		
	}

	private Object handleDelete() {
		String selectedName = this.namesListView.getSelectionModel().getSelectedItem();
		this.theViewModel.deleteStudent(selectedName);
		return null;
	}

	private void handleAboutProgram() {
		JOptionPane.showMessageDialog(null,
			    ABOUT_PROGRAM_DIALOGUE,
			    ABOUT_FACETUTOR_TITLE,
			    JOptionPane.INFORMATION_MESSAGE);
	}

	private void bindGuiComponentsToViewModel() {
		this.studentImageView.imageProperty().bindBidirectional(this.theViewModel.faceImageProperty());
	}
	
	private Object handleNameSelected() {
		boolean isNameCorrect;
		String selectedName = this.namesListView.getSelectionModel().getSelectedItem();
		isNameCorrect = this.theViewModel.checkNameIfCorrect(selectedName);
		System.out.println(selectedName + " Selected!");
		if(isNameCorrect == true) {
			JOptionPane.showMessageDialog(null,
				    isNameCorrect+CORRECT_NAME,
				    NAME_PICTURE_MATCH_TITLE,
				    JOptionPane.INFORMATION_MESSAGE);
		} else if(isNameCorrect == false) {
			JOptionPane.showMessageDialog(null,
				    isNameCorrect+INCORRECT_NAME,
				    NAME_PICTURE_MATCH_TITLE,
				    JOptionPane.ERROR_MESSAGE);
		}
		return null;
	}

	private Object handleNextFaceAction() {
		System.out.println("Next Face!");
		if (this.learningCheckBox.selectedProperty().get() == true) {
			this.theViewModel.nextFaceLearning();
			this.namesListView.getSelectionModel().clearSelection();
		} else if (this.learningCheckBox.selectedProperty().get() == false) {
			this.theViewModel.nextFacePractice();
			this.namesListView.getSelectionModel().clearSelection();
		}
		
		return null;
	}
	
	private void handleImportClassRollAction() {
		FileChooser chooser = this.initializeFileChooser("Read from");
		File inputFile = chooser.showOpenDialog(null);
		
		if (inputFile == null) {
			return;
		}		
		try {
			this.theViewModel.addClassRollFrom(inputFile);
			JOptionPane.showMessageDialog(null,
				    SUCCESSFUL_LOAD_MESSAGE,
				    LOAD_FACETUTOR_DATA_TITLE,
				    JOptionPane.INFORMATION_MESSAGE);
			this.namesListView.setItems(this.theViewModel.getClassRollList());
			this.enableButtons();
			
		} catch (IOException readException) {
			JOptionPane.showMessageDialog(null,
				    READ_DATA_ERROR_MESSAGE,
				    ERROR_DIALOG_TITLE,
				    JOptionPane.ERROR_MESSAGE);
		}	
	}

	private FileChooser initializeFileChooser(String title) {
		FileChooser chooser = new FileChooser();
		
		chooser.getExtensionFilters().add(
						new ExtensionFilter("CSV Files", "*.csv"));
		chooser.setTitle(title);
		return chooser;
	}

	private void enableButtons() {
		this.nextFaceButton.setDisable(false);
		this.deleteCurrentButton.setDisable(false);
	}
	
}