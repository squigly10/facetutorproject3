package edu.westga.facetutor1.view;

import java.io.File;
import java.io.IOException;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.Property;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.image.Image;
import edu.westga.facetutor1.controllers.DeleteStudentController;
import edu.westga.facetutor1.controllers.LoadFaceTutorDataController;
import edu.westga.facetutor1.controllers.MatchPictureAndNameController;
import edu.westga.facetutor1.controllers.NextStudentLearningController;
import edu.westga.facetutor1.controllers.NextStudentPracticeController;
import edu.westga.facetutor1.model.DataMap;
import edu.westga.facetutor1.model.FaceTutorData;
import edu.westga.facetutor1.model.FaceTutorDataMap;

/**
 * FaceTutorViewModel mediates between the view and the rest of the program.
 * 
 * @author Thomas Hightower
 * @version 1.0
 */
public class FaceTutorViewModel {
	
	private static final String DEFAULT_IMAGE = "initial_image.png";
	private ObjectProperty<Image> faceImage;
	private StringProperty studentName;
	private StringProperty faceUrl;
	private String currentUrl;
	
	private ObservableList<String> classRollList;
	private DataMap<String, FaceTutorData> theMap;
	
	private LoadFaceTutorDataController loadController;
	private NextStudentLearningController nextLearningController;
	private NextStudentPracticeController nextPracticeController;
	private MatchPictureAndNameController matchController;
	private DeleteStudentController deleteController;
	/**
	 * Creates a new FaceTutorViewModel object with its properties initialized.
	 * 
	 * @precondition none
	 * @postcondition the object exists
	 */
	public FaceTutorViewModel() {
		Image initialImage = new Image(getClass().getResourceAsStream(DEFAULT_IMAGE));
		this.faceImage = new SimpleObjectProperty<Image>(initialImage);
		
		this.studentName = new SimpleStringProperty();
		this.faceUrl = new SimpleStringProperty();
		this.currentUrl = new String();
		
		this.classRollList = FXCollections.observableArrayList();
		this.theMap = new FaceTutorDataMap();
		
		this.nextLearningController = new NextStudentLearningController();
		this.nextPracticeController = new NextStudentPracticeController();
		this.matchController = new MatchPictureAndNameController();
		this.deleteController = new DeleteStudentController();
	}

	/**
	 * Returns the property that wraps the lines of text.
	 * 
	 * @precondition none
	 * @return the lines of text
	 */
	public ObservableList<String> getClassRollList() {
		return this.classRollList;
	}
	
	/**
	 * Returns the property that wraps the initial image.
	 * 
	 * @precondition none
	 * @return the initial image
	 */
	public StringProperty studentNameStringProperty() {
		return this.studentName;
	}
	
	/**
	 * Returns the property that wraps the initial image.
	 * 
	 * @precondition none
	 * @return the initial image
	 */
	public StringProperty faceUrlStringProperty() {
		return this.faceUrl;
	}
	
	/**
	 * Returns the property that wraps the initial image.
	 * 
	 * @precondition none
	 * @return the initial image
	 */
	public Property<Image> faceImageProperty() {
		return this.faceImage;
	}
	
	/**
	 * Returns the number of students that have been added to 
	 * this view model's collection.
	 * 
	 * @precondition none
	 * @return		 how many students
	 */
	public int size() {
		return this.theMap.size();
	}
	
	/**
	 * Adds the names in the file to the Class Roll List
	 * 
	 * @precondition 	inputFile != null
	 * @postcondition 	the date has been loaded, so that size()
	 * 					equals the number of lines in the file and
	 * 					getTitles() returns all the titles of the
	 * 					movies in the file.       
	 * 
	 * @param	inputFile
	 * 				a text file
	 * @throws	IOException	if the file cannot be read
	 */
	public void addClassRollFrom(File inputFile) throws IOException {
		if (inputFile == null) {
			throw new IllegalArgumentException("File to load is null");
		}
		
		this.loadController = new LoadFaceTutorDataController(inputFile, this.theMap);
		this.loadController.loadFaceTutorData();
		
		this.updateTitleList();
	}
	
	/**
	 * Checks if the name selected matches the current face image
	 * 
	 * @param selectedName 
	 * 
	 */
	public boolean checkNameIfCorrect(String selectedName) {
		boolean isMatch;
		isMatch = this.matchController.checkIfMatch(currentUrl, selectedName, theMap);
		return isMatch;
	}
	
	
	/**
	 * Makes the next student's face appear on the screen
	 * in learning mode
	 * 
	 * @postcondition	the next student's face is on the screen
	 */
	public void nextFaceLearning() {
		String nextFace = this.nextLearningController.goNext(theMap);
		this.setFaceImagePropertyFor(this.theMap.get(nextFace));
	}
	
	/**
	 * Makes the next student's face appear on the screen
	 * in practice mode
	 * 
	 * @postcondition	the next student's face is on the screen
	 */
	public void nextFacePractice() {
		String nextFace = this.nextPracticeController.goNext(theMap);
		this.setFaceImagePropertyFor(this.theMap.get(nextFace));	
	}
	
	/**
	 * Deletes the current student from the Map
	 * 
	 * @param selectedName
	 */
	public void deleteStudent(String selectedName) {
		this.theMap = this.deleteController.DeleteStudent(selectedName, theMap);
		
	}
	
	//****************** private helper methods ***********************

		/*
		 * Stores the face image accessed from the Internet
		 * in this.faceImage so that the face can be displayed in
		 * the view. If the web image can't be read, sets the image
		 * to its default value.
		 */
//	private void setfaceImagePropertyFor(FaceTutorData theStudent) {
//		Image theFace = null;
//		try {
//			theFace = new Image(theStudent.getFaceUrl());
//		} catch (Exception invalidUrl) {
//			theFace = new Image(getClass().getResourceAsStream(DEFAULT_IMAGE));
//		}
//		this.faceImage.setValue(theFace);
//	}
	
	private void setFaceImagePropertyFor(FaceTutorData theStudent) {
		Image theFace = null; 
		try {
			theFace = new Image(theStudent.getFaceUrl());
		} catch (Exception invalidUrl) {
			theFace = new Image(getClass().getResourceAsStream(DEFAULT_IMAGE));
		}
		this.faceImage.setValue(theFace);
		this.currentUrl = theStudent.getFaceUrl();
	}
	
	private void updateTitleList() {
		this.classRollList.clear();
		this.theMap = this.loadController.getTheMap();
		this.classRollList.addAll(this.theMap.keys());
		this.classRollList.sort(null);
		System.out.println(this.theMap.keys());
		
	}
	
}
