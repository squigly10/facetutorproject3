package edu.westga.facetutor1.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * MovieDataMap implements the DataMap interface to 
 * store String/MovieData pairs in memory.
 * 
 * @author CS 1302
 * @version Spring, 2015
 */
public class FaceTutorDataMap implements DataMap<String, FaceTutorData> {
	
	private Map<String, FaceTutorData> theMap;
	
	/**
	 * Creates a new FaceTutorDataMap with an empty map.
	 * 
	 * @precondition none
	 * @postcondition size() == 0
	 */
	public FaceTutorDataMap() {
		this.theMap = new HashMap<String, FaceTutorData>();
	}

	@Override
	public void add(String aKey, FaceTutorData aValue) {
		if (aKey == null) {
			throw new IllegalArgumentException("The key was null");
		}
		if (aValue == null) {
			throw new IllegalArgumentException("The value was null");
		}
		if (this.contains(aKey)) {
			throw new IllegalArgumentException("Key value is already in the map");
		}
		
		this.theMap.put(aKey, aValue);		
	}	

	@Override
	public FaceTutorData get(String aKey) {
		if (aKey == null) {
			throw new IllegalArgumentException("The key was null");
		}
		if (!this.contains(aKey)) {
			throw new IllegalArgumentException("Key value is not in the map");
		}
		
		return this.theMap.get(aKey);
	}

	@Override
	public boolean contains(String aKey) {
		if (aKey == null) {
			throw new IllegalArgumentException("The key was null");
		}
		
		return this.theMap.containsKey(aKey);
	}

	@Override
	public int size() {
		return this.theMap.size();
	}

	@Override
	public List<FaceTutorData> values() {
		return new ArrayList<FaceTutorData>(this.theMap.values());
	}

	@Override
	public List<String> keys() {
		return new ArrayList<String>(this.theMap.keySet());
	}
	
	@Override
	public void remove(String aKey, FaceTutorData aValue) {
		this.theMap.remove(aKey, aValue);
	}

}