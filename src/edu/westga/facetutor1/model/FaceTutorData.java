package edu.westga.facetutor1.model;

/**
 * FaceTutorDate objects represent the name and face url of a student.
 * 
 * @author Thomas Hightower
 * @version	1.0
 */
public class FaceTutorData {

	private final String name;
	private final String faceUrl;

	/**
	 * Creates a new FaceTutorDate object with the specified
	 * name and face url.
	 * 
	 * @precondition 	name != null && name.length() > 0 &&
	 * 				    faceUrl != null && faceUrl.length() > 0
	 * @postcondition
	 * 
	 * @param name			the name of the student
	 * @param faceUrl		the URL of the face image
	 */
	public FaceTutorData(String name, String faceUrl) {
		if (name == null) {
			throw new IllegalArgumentException("Name is null");
		}
		if (name.length() == 0) {
			throw new IllegalArgumentException("Name is empty");
		}
		if (faceUrl == null) {
			throw new IllegalArgumentException("faceUrl is null");
		}
		if (faceUrl.length() == 0) {
			throw new IllegalArgumentException("faceUrl is empty");
		}
		
		this.name = name;
		this.faceUrl = faceUrl;
	}

	/**
	 * Returns the name of the student.
	 * 
	 * @precondition	none
	 * @return the name
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * Returns the URL of the face image.
	 * 
	 * @precondition	none
	 * @return the face URL
	 */
	public String getFaceUrl() {
		return this.faceUrl;
	}
	
}
