package edu.westga.facetutor1;

import java.io.IOException;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;

/**
 * FaceTutortApplication extends the JavaFX Application class to build the GUI
 * and start program execution.
 * 
 * @author Thomas Hightower
 * @version Spring, 2015
 */
public class FaceTutorApplication extends Application{
	
	private static final String WINDOW_TITLE = "Face Tutor BETA";
	private static final String GUI_FXML = "view/FaceTutorGui.fxml";

	/**
	 * Constructs a new Application object for the Face Tutor
	 * program.
	 * 
	 * @precondition	none
	 * @postcondition	the object is ready to execute
	 */
	public FaceTutorApplication() {
		super();
	}

	@Override
	public void start(Stage primaryStage) {
		try {
			Pane pane = this.loadGui();
			Scene scene = new Scene(pane);
			primaryStage.setScene(scene);
			primaryStage.setTitle(WINDOW_TITLE);
			primaryStage.show();
		} catch (IllegalStateException | IOException anException) {
			anException.printStackTrace();
		}
	}

	private Pane loadGui() throws IOException {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getResource(GUI_FXML));
		return (Pane) loader.load();
	}

	/**
	 * Launches the application.
	 * 
	 * @param args
	 *            not used
	 */
	public static void main(String[] args) {
		launch(args);
	}
}
